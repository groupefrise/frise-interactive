# Frise Interactive (DLL9)

Le projet de Frise Interactive est un projet visant à fournir aux étudiants et ressources pédagogiques de l'université d'Evry Val d'Essone (Université Paris-Sac) une frise chronologique représentant toutes les filières étudiées dans cette université.  
Ce projet est un projet réalisé par des étudiants en Master 2 MIAGE dans cette même université et comptant pour la validation de leur module "Développement de Logiciel Libre".  
Ce document présente à la fois le projet, les réalisations et la manière de reprendre un tel projet pour les étudiants suivants.  

## Choix d'architecture

Pour réaliser cette application nous avons décidé d'utiliser une architecture 3 tiers avec pour la base de données PostgreSQL, pour le moteur de l'application (serveur) Spring (Java) et pour l'interface utilisateur l'utilisation du Framework AngularJS.  
Nous avons également développé se projet dans l'IDE Netbeans permettant de facilement avoir un environnement avec un serveur Tomcat pour faire les tests et les déploiements.  

### Base de données

Pour faire fonctionner la base de données, il suffit d'installer PostgreSQL (n'importe quelle version depuis la 9.4) et de lancer le script présent dans la branche BDD.  
L'ajout des données de tests peut être fait par n'importe qui via l'interface Postgresql ou par des scripts d'insertion SQL.  
Le modèle de données a pour but de simplifier la création des services et l'accès aux données pour de meilleurs temps de réponse.  

### Serveur Spring (Java)

Pour le Serveur nous avons opté pour un serveur Spring qui permet de facilement gérer les accès avec PostgreSQL et de définir des vraies règles de gestion autour des données.  
Ces règles de gestion sont importantes pour que l'utilisateur puisse correctement gérer ses données et également pour la reprise des développements par la suite avec l'ajout de fonctionnalités.  
L'autre avantage de Spring est de permettre la création simple d'une API sur laquelle n'importe quel utilisateur peut se connecter et interroger la base même sans interface graphique.  

### Interface Utilisateur

Nous avons utilisé Bootstrap pour la mise en page et l'ergonomie ainsi qu'AngularJS pour le moteur front de l'application.  

## Installation et déploiement

Pour installer le serveur et l'interface, il suffit de pool les branches server (pour le serveur) et developpement (pour l'interface) Vous aurez alors dans votre IDE toutes les parties indépendantes du projet.  
Pour lancer le serveur :
- Build le projet une fois installé avec la fonction "Build with dependencies" de Netbeans 
- Run le projet pour le lancer et saisir les adresses des différentes API pour accéder aux données désirées
 
Pour l'interface utilisateur
- Une fois le serveur lancé "run" l'application front pour voir apparaître l'interfaceavec les données récupérés via le serveur.
- Si les données n'aparraissent pas, modifier les informations dans le fichier properties.js pour avoir les bonnes informations de connexion (pour et adresse notamment)

## Fonctionnement des API

Le serveur est conçu de manière à créer simplement les API nécessaires et utilisables par toute application souhaitant s'y connecter.  
Exemple pour la récupération des mouvements artistiques :
- Structure des liens :
  * http://localhost:8084/FriseBack/rest/... --> Lien de base sur lequel toutes les API sont codées
  * http://localhost:8084/FriseBack/rest/mouvements --> Ajout du type de données recherché (mouvements, domaines, personnes, evenements, etc.)
  * http://localhost:8084/FriseBack/rest/mouvements?id=1 --> Choix spécifiques avec les services particuliers dans chaque type de données cité précédemment.
  
Voir les détails avec l'installation de l'application.

  
## Interface utilisateur 

L'interface utilisateur se connecte aux API comme n'importe quelle application tierce pourrait le faire et utilise les données de la manière qui lui convient.
Voir test et démonstration en installant le projet
